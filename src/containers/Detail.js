import React, { Component } from 'react';
import axios from 'axios';
import { Link } from "react-router-dom";

class Detail extends Component {
    constructor(props) {
      super(props);
  
      this.state = {
        list: []
      };
    }
  
    componentDidMount() {
        let id = this.props.match.params.id;
        axios
            .get('http://139.180.139.69:3008/api/items/detail/' + id)
            .then(res => {
                this.setState({
                    list: res.data.data
                });
            })
            .catch(err => {
                console.log(err);
            });
    }
  
    render() {
        let item = this.state;
        return(
            <div className="container mt-4 mb-4">
                <div className="mb-3">
                    <h5><b>{item.list.name}</b></h5>
                </div>
                    <div className="detail-parent">
                        <img src={item.list.image} className="img-max" alt="" />
                        <div className="detail-child pt-3 pb-2">
                            <p><b>{item.list.name}</b></p>
                            <p>{item.list.description}</p>
                            <div className="rating">
                                <div className="pl-1 pr-1">
                                    <i className="fa fa-star" style={{width: '100%'}} aria-hidden="true"></i>
                                </div>
                                <div className="pl-1 pr-1">
                                    <i className="fa fa-star" style={{width: '100%'}} aria-hidden="true"></i>
                                </div>
                                <div className="pl-1 pr-1">
                                    <i className="fa fa-star" style={{width: '100%'}} aria-hidden="true"></i>
                                </div>
                                <div className="pl-1 pr-1">
                                    <i className="fa fa-star" style={{width: '100%'}} aria-hidden="true"></i>
                                </div>
                            </div>
                            <div className="mt-4 mb-4 btn-pay text-center">
                                <button type="submit" class="btn btn-custom">
                                    <Link style={{ color: 'white' }} to="/payment">Order Now</Link>
                                </button>
                            </div> 
                        </div>
                    </div>
            </div>
        )
    }
}
  
export default Detail;
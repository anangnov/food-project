import React, { Component } from 'react';
import axios from 'axios';
import { Link } from "react-router-dom";

class Home extends Component {
    constructor(props) {
      super(props);
  
      this.state = {
        list: []
      };
    }
  
    componentWillMount() {
      axios
        .get('http://139.180.139.69:3008/api/items')
        .then(res => {
          this.setState({
            list: res.data.data
          });
        })
        .catch(err => {
          console.log(err);
        });
    }
  
    render() {
        let item = this.state;
        return(
            <div className="container mt-4 mb-4">
                <div className="mb-3">
                    <h5><b>Food Menu</b></h5>
                </div>
                <div className="row">
                    {
                        item.list.map((key, i) => {
                            return(
                                <div className="col-md-6 mb-4">
                                    <Link to={"/detail/" + key.id}>
                                    <div className="card card-custom shadow rounded">
                                        <img src={key.image} className="card-img-top img-max" alt="" />
                                        <div className="card-body p-2 text-center">
                                            <p className="card-title">{key.name}</p>
                                        </div>
                                    </div>
                                    </Link>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}
  
export default Home;
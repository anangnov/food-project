import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Header from "../components/Header";
import Home from "../containers/Home";
import Detail from "../containers/Detail";
import Login from "../components/Login";
import Payment from "../components/Payment";

class Routes extends Component {
    render() {
        let pathname = window.location.pathname;
        return (
            <Router>
                {
                    pathname === '/' ? (
                        <div className="main-container">
                            <Switch>
                                <Route exact path="/" component={Login} />
                            </Switch>
                        </div>
                    ) : (
                        <div className="main-container">
                            <Header title="Food Delivery App" />
                            <Switch>
                                <Route exact path="/home" component={Home} />
                                <Route exact path="/detail/:id" component={Detail} />
                                <Route exact path="/payment" component={Payment} />
                            </Switch>
                        </div>
                    )
                }
            </Router>
        );
    }
}

export default Routes;
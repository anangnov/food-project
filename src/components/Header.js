import React from 'react';

const Header = (props) => {
    return(
        <div>
            <nav className="navbar navbar-expand-lg navbar-light nav-parent">
                <a className="navbar-brand text-center" href="/home" style={{width: '100%'}}>
                    <b>{props.title}</b>
                </a>
            </nav>
            <nav className="navbar navbar-expand-lg navbar-light nav-child p-1">
                <div className="icon-parent">
                    <div className="pl-2 pr-2">
                        <i className="fa fa-home" style={{width: '100%'}} aria-hidden="true"></i>
                    </div>
                    <div className="pl-2 pr-2">
                        <i className="fa fa-cube" style={{width: '100%'}} aria-hidden="true"></i>
                    </div>
                    <div className="pl-2 pr-2">
                        <i className="fa fa-money" style={{width: '100%'}} aria-hidden="true"></i>
                    </div>
                    <div className="pl-2 pr-2">
                        <i className="fa fa-user" style={{width: '100%'}} aria-hidden="true"></i>
                    </div>
                    <div className="pl-2 pr-2">
                        <i className="fa fa-hashtag" style={{width: '100%'}} aria-hidden="true"></i>
                    </div>
                </div>
            </nav>
        </div>
    )
}

export default Header;
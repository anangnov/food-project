import React from 'react';
import img3 from '../assets/images/food-3.jpg';

const Login = (props) => {
    return(
        <div className="container">
            <div className="">
                <img src={img3} className="img-login" alt="" />
                <div className="mt-4 mb-3">
                    <h5>Sign In</h5>
                    <div>
                        <form>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" />
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1" />
                                <label class="form-check-label" for="exampleCheck1">Check me out</label>
                            </div>
                            <button type="submit" class="btn btn-custom">
                                <a style={{ color: 'white' }} href="/home">Submit</a>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login;
import React from 'react';

const Checkout = (props) => {
    return(
        <div className="container mt-4 mb-4">
            <div className="mb-3">
                <h5><b>Payment</b></h5>
            </div>
            <div className="detail-parent">
                <div className="">
                    <h3>Payment</h3>
                    <label for="fname">Accepted Cards</label>
                    <div class="icon-container mb-4">
                        <i class="fa fa-cc-visa pl-1 pr-2" style={{color:'navy'}}></i>
                        <i class="fa fa-cc-amex pl-1 pr-2" style={{color:'blue'}}></i>
                        <i class="fa fa-cc-mastercard pl-1 pr-2" style={{color:'red'}}></i>
                        <i class="fa fa-cc-discover pl-1 pr-2" style={{color:'orange'}}></i>
                    </div>
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Name Card" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Credit card number" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Expired Month" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Address" />
                        </div>
                    </form>
                    <div className="mt-4 mb-4 btn-pay text-center">
                        <button type="submit" class="btn btn-custom">
                            <a style={{ color: 'white' }} href="/home">Pay Now</a>
                        </button>
                    </div> 
                </div>
            </div>
        </div>
    )
}

export default Checkout;